#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    """
    Display video stream of all cameras.
    """
    with open("conf/cameras.txt") as f :
        cams = f.readlines()
    return render_template('index.html',cams=cams)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=5500)
